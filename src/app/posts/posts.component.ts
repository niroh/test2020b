import { AuthService } from './../services/auth.service';
import { PostsService } from './../services/posts.service';
import { Component, OnInit } from '@angular/core';
import { Post } from '../interfaces/post';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit { 

  constructor(public postsservice:PostsService,
              public activatedroute:ActivatedRoute,
              public authservice: AuthService) { }

  posts$: Post[];
  comments$: Comment[];
  title:string;
  body:string;
  id:number;
  test:number;
  text:string;
  userId:string;
  likes:number = 0;

  ngOnInit() {
     this.postsservice.getPosts().subscribe(
      data => {
        this.posts$ = data;
      }
    )
     this.postsservice.getComments().subscribe(
      comment => {
        this.comments$ = comment;
      }
    )
    this.authservice.user.subscribe(
      user=> {
        this.userId = user.uid;
      }
    )

  }

  pushToFB(title:string, body:string){
    this.postsservice.savePost(title,body,this.userId )
    console.log(this.text)
    }

}
