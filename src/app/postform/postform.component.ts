import { Component, OnInit } from '@angular/core';
import { PostsService } from '../services/posts.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-postform',
  templateUrl: './postform.component.html',
  styleUrls: ['./postform.component.css']
})
export class PostformComponent implements OnInit {

  constructor(private postsservice:PostsService, 
    private router:Router,
    private route:ActivatedRoute,
    private authservice:AuthService) { }

    title:string;
    body:string;
    id:string;
    isEdit:boolean = false;
    buttonText:string = "Add Post";
    userId: string;

    ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authservice.user.subscribe(
    user => {
    this.userId = user.uid;

    if(this.id){
    this.isEdit = true;
    this.buttonText = "Update Post";
    this.postsservice.postForUpdate(this.id, this.userId).subscribe(
      post => {
        this.title = post.data().title;
        this.body = post.data().body;
        })
      }
    }
    )
  }

  onSubmit(){ 
    if(this.isEdit){
      console.log('edit mode');
      this.postsservice.updatePost(this.userId, this.id,this.title,this.body);
    } else {
      console.log('In onSubmit');
      this.postsservice.addPost(this.userId,this.title,this.body)
    }
    this.router.navigate(['/savedposts']);  
  }  


}
