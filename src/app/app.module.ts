import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule} from '@angular/forms';


//firebase
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';


//firestore
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';

//auth
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';



//angular material
import { MatSliderModule } from '@angular/material/slider';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//components
import { NavComponent } from './nav/nav.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { ClassifiedComponent } from './classified/classified.component';
import { DocFormComponent } from './doc-form/doc-form.component';
import { PostsComponent } from './posts/posts.component';
import { SavedclassComponent } from './savedclass/savedclass.component';
import { SavedpostsComponent } from './savedposts/savedposts.component';
import { PostformComponent } from './postform/postform.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { TempformComponent } from './tempform/tempform.component';
import { TodosComponent } from './todos/todos.component';
import { MytodoComponent } from './mytodo/mytodo.component';
import { UsersComponent } from './users/users.component';
import { RegisteredusersComponent } from './registeredusers/registeredusers.component';
import { TodosformComponent } from './todosform/todosform.component';


const appRoutes: Routes = [
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'notfound', component: NotfoundComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'savedposts', component: SavedpostsComponent },
  { path: 'docform', component: DocFormComponent },
  { path: 'classified', component: ClassifiedComponent },
  { path: 'savedclass', component: SavedclassComponent },
  { path: 'postform', component: PostformComponent },
  { path: 'postform/:id', component: PostformComponent },
  { path: 'temperatures/:city', component: TemperaturesComponent},
  { path: 'tempform', component: TempformComponent },
  { path: 'mytodo', component: MytodoComponent },

  { path: 'welcome', component: WelcomeComponent },
  { path: 'todos', component: TodosComponent },
  { path: 'users', component: UsersComponent },
  { path: 'registeredusers', component: RegisteredusersComponent },
  { path: 'todosform', component: TodosformComponent },

  { path: "", redirectTo: '/welcome', pathMatch: 'full'},
  { path: "**", redirectTo: '/welcome',pathMatch: 'full'},


];


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SignupComponent,
    LoginComponent,
    WelcomeComponent,
    NotfoundComponent,
    ClassifiedComponent,
    DocFormComponent,
    PostsComponent,
    SavedclassComponent,
    SavedpostsComponent,
    PostformComponent,
    TemperaturesComponent,
    TempformComponent,
    TodosComponent,
    MytodoComponent,
    UsersComponent,
    RegisteredusersComponent,
    TodosformComponent,
    
  ],
  imports: [

    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),

    BrowserModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
  
    BrowserModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatCardModule,
    MatListModule,
    MatIconModule,
    MatSidenavModule,
    MatButtonModule,
    MatToolbarModule,
    LayoutModule,
    MatSliderModule,
    HttpClientModule,
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    FormsModule,
    MatCheckboxModule


  ],
  providers: [
    AngularFireAuth,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
