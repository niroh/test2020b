import { UsersService } from './../services/users.service';
import { User } from './../interfaces/user';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(public usersservice:UsersService,
              public authservice:AuthService) { }

  users$ : Observable<any[]>;
  password:string = '12345678';
  userId:string;

  ngOnInit() {
   this.usersservice.getUsers().subscribe(
     data => {
       this.users$ = data
     }
   )
   this.authservice.user.subscribe(
    user => {
      this.userId = user.uid;
    }

  )
  }

  pushToFB(id:number, name:string, email:string){
    this.usersservice.saveUser(id,name,email ,this.userId)
    }

    onSubmit(email:string){
      this.authservice.signup(email, this.password);
      }

}
