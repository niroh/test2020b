import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tempform',
  templateUrl: './tempform.component.html',
  styleUrls: ['./tempform.component.css']
})
export class TempformComponent implements OnInit {

  constructor(private router: Router) { }

  cities:object[] = [{id:1, name:'jerusalem'},{id:2, name:'London'},
                     {id:3, name:'Paris'}, {id:4, name:'non-exitent'}];
  temperature:number;
  city:string; 

  onSubmit(){
    this.router.navigate(['/temperatures', this.city]);
  }

  ngOnInit() {
  }

}
