import { Component, OnInit } from '@angular/core';
import { TodosService } from '../services/todos.service';
import { AuthService } from '../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-todosform',
  templateUrl: './todosform.component.html',
  styleUrls: ['./todosform.component.css']
})
export class TodosformComponent implements OnInit {

  constructor(private todosservice:TodosService,
    private authservice:AuthService, 
    private router:Router,
    private route: ActivatedRoute) { }

    title:string;
    userId:string;
    complete:string = 'false';
    
 

  ngOnInit() {
    this.authservice.user.subscribe(
      user=> {
        this.userId = user.uid;
      }
    )
  }

  onSubmit(){ 
      this.todosservice.addTodo(this.complete, this.title, this.userId)
    
    this.router.navigate(['/todos']);  
  }  
}
