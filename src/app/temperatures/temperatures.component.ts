import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TempService } from '../services/temp.service';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/weather';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  constructor(public route: ActivatedRoute, 
              public tempService:TempService ) { }

  likes:number = 0;
  temperature:number;
  city:string;
  image:String;
            
            
  tempData$: Observable<Weather>;
            
   addLikes(){
    this.likes ++ //likes that belongs to this section...
     }
            
                       

  ngOnInit() {
        //this.temperature = this.route.snapshot.params.temp;
       // this.city= this.route.snapshot.params.city;
           this.city = this.route.snapshot.params.city;
    // לא מעניין כי אנחנו הולכים לקרוא את זה עכשיו מהשרת
    // this.temperature = this.router.snapshot.params.temp;
        this.tempData$ = this.tempService.searchWeatherData(this.city);
        this.tempData$.subscribe(
          data => {
            console.log(data);
            this.temperature = data.temperature;
            this.image = data.image;

          } 
        )
    
  }

}
