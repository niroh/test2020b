import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../services/auth.service';
import { UsersService } from '../services/users.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-registeredusers',
  templateUrl: './registeredusers.component.html',
  styleUrls: ['./registeredusers.component.css']
})
export class RegisteredusersComponent implements OnInit {

  constructor(public usersservice:UsersService,
              public authservice:AuthService,
              public db:AngularFirestore) { }

 users$:Observable<any>;
 userId:string;
 
  ngOnInit() {
    this.authservice.user.subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
    this.users$ = this.usersservice.getUser(this.userId);
    console.log('there is '+this.users$);
       }
    )
  }

}
