export interface User {
    uid:string,
    email?: string | null,
    name?:string | null,
    password?:string,
    photoUrl?: string,
    displayName?:string

}
