import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { User } from '../interfaces/user';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(public http:HttpClient,
              public router:Router,
              public db:AngularFirestore,
              public authsetvice:AuthService) { 
              }

    private usersURL = "https://jsonplaceholder.typicode.com/users";

    user: Observable<User | null>
    userCollection: AngularFirestoreCollection = this.db.collection('users');
    insideCollection: AngularFirestoreCollection;



    getUsers():Observable<any>{
      return this.http.get<User>(this.usersURL)
    }

    saveUser(id:number, name:string, email:string, userId:string){
      const user = {id:id, name:name, email:email}
      this.db.collection('usersTest').add(user);
     //this.userCollection.doc(userId).collection('usersTest').add(user);
    // this.router.navigate(['/savedposts'])
   }

   getUser(userId):Observable<any[]>{
    this.userCollection = this.db.collection(`usersTest`);
        console.log('Books collection created');
        return this.userCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            data.id = a.payload.doc.id;
            return { ...data };
          }))
        ); 
  }
  

 
}
