import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/weather';
import { WeatherRaw } from '../interfaces/weather-raw';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TempService {

  constructor(private http:HttpClient) { }

  private URL = "http://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "3c29bddc6b78b9e3b102e26d7e4bc102";
  private IMP = "&units=metric";

  searchWeatherData(cityName:String): Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
    .pipe(map(data=>this.transformWeatherData(data))
    )

  }
  private transformWeatherData(data:WeatherRaw):Weather{
    return {
      name:data.name,
      country:data.sys.country,
      image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon,

    }

  }



}
