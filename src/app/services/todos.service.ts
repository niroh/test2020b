import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Todo } from '../interfaces/todo';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constructor(public http:HttpClient,
              public router:Router,
              public db:AngularFirestore) { }

  public todosURL = "https://jsonplaceholder.typicode.com/todos";

  userCollection: AngularFirestoreCollection = this.db.collection('users');
  todoCollection: AngularFirestoreCollection;

  getTodos(){
    return this.http.get<Todo[]>(this.todosURL)
  }

  saveTodo(userId:string, title:string, completed:string){
    const todo = {title:title, completed:completed}
    //this.db.collection('posts').add(post);
    this.userCollection.doc(userId).collection('todos').add(todo);
    this.router.navigate(['/mytodo'])
  }

  getTodo(userId):Observable<any[]>{
    this.todoCollection = this.db.collection(`users/${userId}/todos`);
        console.log('todo collection created');
        return this.todoCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            data.id = a.payload.doc.id;
            return { ...data };
          }))
        ); 
  }

  changeCheckBox(id:string, completed:string, userId:string){
    //console.log(completed)
    if(completed){
      this.db.doc(`users/${userId}/todos/${id}`).update({
        completed:false,
      })
    }
    else{
      this.db.doc(`users/${userId}/todos/${id}`).update({
        completed:true,
      })
    }
  }

  updatePost(userId:string, id:string, title:string, author:string){
    this.db.doc(`users/${userId}/posts/${id}`).update(
      {title:title,
      author:author}
    )

  }

  addTodo(complete:string, title:string, userId:string,){
   // console.log('In add books');
    const todo = {complete:complete, title:title}
    //this.db.collection('books').add(book)  
    this.userCollection.doc(userId).collection('todos').add(todo);
  } 

  changeDone(id:string, complete:string, userId:string){
    if(complete){
            this.db.doc(`users/${userId}/todos/${id}`).update({
              complete:'false'
            })
          }
          else{
            this.db.doc(`users/${userId}/todos/${id}`).update({
              complete:'true'
            })
          }
        }

  



  

}
