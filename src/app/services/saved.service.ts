import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SavedService {

  constructor(public http:HttpClient,
    public db:AngularFirestore,
    public router:Router) { }

    userCollection: AngularFirestoreCollection = this.db.collection('users');
    savedClassCollection: AngularFirestoreCollection;
    userId:string;

    getSavedClass(userId): Observable<any>{
     // return this.db.collection('saved').valueChanges(({idField:'id'}));
       this.savedClassCollection = this.db.collection(`users/${userId}/saved`);
          //console.log('Books collection created');
          return this.savedClassCollection.snapshotChanges().pipe(
            map(actions => actions.map(a => {
              const data = a.payload.doc.data();
              data.id = a.payload.doc.id;
              return { ...data };
            }))
          );       
      }
    
      saveClass(body:string,aiCat:string, manCat:string, userId:string){
        const saved = {body:body, aiCat:aiCat, manCat:manCat}
         //this.db.collection('saved').add(saved);
         this.userCollection.doc(userId).collection('saved').add(saved);
         this.router.navigate(['/savedclass'])
      }
}
