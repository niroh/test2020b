import { AuthService } from './../services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { TodosService } from './../services/todos.service';
import { Component, OnInit } from '@angular/core';
import { Todo } from '../interfaces/todo';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  constructor(public todosservice:TodosService,
              public activatedroutes:ActivatedRoute,
              public authservice:AuthService) { }

  todos$:Observable<any[]>;
  //comments$: Comment[];
  title:string;
  //userId:string;
  complete:string;
  userId:string;
  
  ngOnInit() {
    this.authservice.user.subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.todos$ = this.todosservice.getTodo(this.userId); 

      }

    )

  }

/*
  addToList(title:string, completed:string){
    console.log(completed)
    console.log(title)
    this.todosservice.saveTodo(this.userId, title, completed)
    }
*/
  changeState(id:string){
      this.todosservice.changeDone(id, this.complete, this.userId);
    }

}
